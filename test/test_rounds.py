import datetime

from services.db.database import DbSingleton, Round, RoundImport, InvestmentImport, InvestorImport
from services.rounds import Rounds


def test_round_import_with_lead_investor():
    db = DbSingleton.get_instance()
    db.prep_test()

    InvestorImport.create(
        uuid = "bbbb",
        name = "Nick Weber"
    )
    InvestorImport.create(
        uuid = "cccc",
        name = "vester1"
    )
    InvestorImport.create(
        uuid = "dddd",
        name = "vester2"
    )

    InvestmentImport.create(
        uuid = "aaaa",
        name = "Nick Weber",
        type = "person",
        permalink = "test-name",
        cb_url = 'https://hi.com',
        rank = "100",
        created_at = datetime.date.today(),
        updated_at = datetime.date.today(),
        funding_round_uuid = "asdf",
        funding_round_name = "round1",
        investor_uuid = "bbbb",
        investor_name = "Nick Weber",
        investor_type = "person",
        is_lead_investor = True
    )
    
    InvestmentImport.create(
        uuid = "hjkl",
        name = "nother vester1",
        type = "person",
        permalink = "test-name",
        cb_url = 'https://hi.com',
        rank = "100",
        created_at = datetime.date.today(),
        updated_at = datetime.date.today(),
        funding_round_uuid = "asdf",
        funding_round_name = "round1",
        investor_uuid = "cccc",
        investor_name = "vester1",
        investor_type = "person",
        is_lead_investor = False
    )
    
    InvestmentImport.create(
        uuid = "vbnm",
        name = "nother vester2",
        type = "person",
        permalink = "test-name",
        cb_url = 'https://hi.com',
        rank = "100",
        created_at = datetime.date.today(),
        updated_at = datetime.date.today(),
        funding_round_uuid = "asdf",
        funding_round_name = "round1",
        investor_uuid = "dddd",
        investor_name = "vester2",
        investor_type = "person",
        is_lead_investor = False
    )

    RoundImport.create(
            uuid='asdf',
            name='round1',
            type='person',
            permalink='test-name',
            cb_url='https://hi.com',
            rank=100,
            created_at=datetime.date.today(),
            updated_at=datetime.date.today(),
            country_code='US',
            # TODO: other properties
            checked=False,
            raised_amount_usd=150000000
        )

    RoundImport.create(
            uuid='zxcv',
            name='vester1',
            type='person',
            permalink='test-name',
            cb_url='https://hi.com',
            rank=100,
            created_at=datetime.date.today(),
            updated_at=datetime.date.today(),
            country_code='US',
            # TODO: other properties
            checked=False,
            raised_amount_usd=150000000
        )

    # call the service you'll write
    Rounds().create_new_rounds()

    # NOTE: the complier bitches unless you add database=None to Peewee queries. :facepalm:
    cb_round = Round.select().first(database=None)
    #print(cb_round.Lead_Investor__c)
    assert cb_round.Lead_Investor__c == 'Nick Weber'
    #assert True
    assert cb_round.Amount__c > 500000


def test_round_import_with_unqualified_rounds():
    # TODO: write a test to show that given two RoundImports, one with a qualifying `Amount___c` and another without
    # that the application properly skips the unqualified round import.
    #pass
    db = DbSingleton.get_instance()
    db.prep_test()

    # create mock objects
    RoundImport.create(
        uuid='qwer',
        name='Varth Dader',
        type='Sith Master',
        permalink='test-name',
        cb_url='https://hi.com',
        rank=100,
        created_at=datetime.date.today(),
        updated_at=datetime.date.today(),
        country_code='US',
        checked=False,
        raised_amount_usd=100000000
    )

    RoundImport.create(
        uuid='uiop',
        name='Suke Lywalker',
        type='Jedi',
        permalink='test-name',
        cb_url='https://hi.com',
        rank=100,
        created_at=datetime.date.today(),
        updated_at=datetime.date.today(),
        country_code='US',
        checked=False,
        raised_amount_usd=420000
    )

    Rounds().create_new_rounds()
    cb_round = Round.select().count(database=None)
    assert cb_round == 1
