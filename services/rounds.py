from services.crunchbase.rounds import CrunchbaseRounds
from services.db.database import Round, InvestmentImport, InvestorImport


class Rounds():
    def create_new_rounds(self):
        batch = self.__round_import_batch()
        while len(batch) > 0:
            for round_import in batch:
                self.__process_round_import(round_import)
            batch = self.__round_import_batch()

    def __round_import_batch(self):
        return CrunchbaseRounds().unchecked_round_imports()

    def __investment_import_batch(self,uuid):
        return CrunchbaseRounds().get_investment_imports(uuid)

    def __get_lead_investor_name(self,uuid):
        return CrunchbaseRounds().get_investor_name(uuid)

    def __process_round_import(self, round_import):
        investments = self.__investment_import_batch(round_import.uuid)
        lead_investor = ""
        for i in investments:
            if i.is_lead_investor == True:
                lead_investor = self.__get_lead_investor_name(i.investor_uuid)
                break
        if round_import.raised_amount_usd is not None and round_import.raised_amount_usd > 500000:
            round = Round.create(
                Amount__c=round_import.raised_amount_usd,
                Lead_Investor__c=lead_investor
            )
        round_import.checked = True
        round_import.save()
