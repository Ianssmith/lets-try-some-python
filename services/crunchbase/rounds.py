from services.db.database import RoundImport, InvestmentImport, InvestorImport


class CrunchbaseRounds():
    def unchecked_round_imports(self, limit=40):
        return (
            RoundImport
            .select()
            .where(RoundImport.checked == False)
        )

    def get_investment_imports(self, uuid):
            return (
                InvestmentImport
                .select()
                .where(InvestmentImport.funding_round_uuid == uuid)
            )

    def get_investor_name(self, uuid):
            return (
                InvestorImport
                .select()
                .where(InvestorImport.uuid == uuid).first().name
            )